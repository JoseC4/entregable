import { LitElement, html } from 'lit-element';

class PersonaFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
             <nav class="navbar navbar-dark bg-dark">
                <span class="navbar-brand mb-0 h1">@Persona App 2021</span>
            </nav>
        `;
    }
}

customElements.define('persona-footer', PersonaFooter)