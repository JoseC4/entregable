import { LitElement, html } from 'lit-element';

class PersonaHeader extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <nav class="navbar navbar-dark bg-dark">
                <span class="navbar-brand mb-0 h1"><h1>App Persona</h1></span>
            </nav>
        `;
    }
}

customElements.define('persona-header', PersonaHeader)